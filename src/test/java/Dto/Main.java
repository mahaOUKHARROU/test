package Dto;

import java.net.URI;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;

public class Main {
	public static void main(String[] args) throws URISyntaxException, ParseException {
		RestTemplate rt = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		String dateString = format.format(new Date());
		String baseUrl = "https://www.eversports.de/widget/api/slot?facilityId=68673&sport=tennis&startDate="
				+ dateString
				+ "&courts%5B%5D=55100&courts%5B%5D=55101&courts%5B%5D=55076&courts%5B%5D=55077&courts%5B%5D=55078&courts%5B%5D=55079&courts%5B%5D=55080&courts%5B%5D=55081&courts%5B%5D=55082&courts%5B%5D=55083&courts%5B%5D=55100&courts%5B%5D=55101&courts%5B%5D=55076&courts%5B%5D=55077&courts%5B%5D=55078&courts%5B%5D=55079&courts%5B%5D=55080&courts%5B%5D=55081&courts%5B%5D=55082&courts%5B%5D=55083&courts%5B%5D=55100&courts%5B%5D=55101&courts%5B%5D=55076&courts%5B%5D=55077&courts%5B%5D=55078&courts%5B%5D=55079&courts%5B%5D=55080&courts%5B%5D=55081&courts%5B%5D=55082&courts%5B%5D=55083";
		URI uri = new URI(baseUrl);
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.add("user-agent",
				"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
		HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
		ResponseEntity<String> res = rt.exchange(uri, HttpMethod.GET, entity, String.class);
		Gson g = new Gson();
		Slots slots = g.fromJson(res.getBody(), Slots.class);
		List<SlotRecup> slotRecup = convertToRecup(slots);
		String json = new Gson().toJson(slotRecup);
		System.out.println(json);
	}

	private static List<SlotRecup> convertToRecup(Slots slots) throws ParseException {
		List<SlotRecup> slotRecups = new ArrayList<SlotRecup>();
		for (Slot slo : slots.getSlots()) {
			SlotRecup sloRecup = new SlotRecup();
			sloRecup.setCenterId("tb4wwe");
			sloRecup.setFacilityId(slo.getCourt());
			sloRecup.setIsAvailable(slo.getPresent());
			Date date1 = new SimpleDateFormat("yyyy-MM-dd").parse(slo.getDate());
			Calendar cal = Calendar.getInstance();
			cal.setTime(date1);
			cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(slo.getStart().substring(0, 2)));
			cal.set(Calendar.MINUTE, Integer.parseInt(slo.getStart().substring(2, 4)));
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
			sloRecup.setStartTime(df.format(cal.getTime()));
			cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(slo.getStart().substring(0, 2)) + 1);
			sloRecup.setEndTime(df.format(cal.getTime()));
			slotRecups.add(sloRecup);
		}
		return slotRecups;
	}

}
